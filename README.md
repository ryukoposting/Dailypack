## Dailypack

A bare-bones Continuous Delivery (CD) application for enthusiasts

### How does Dailypack work?

Dailypack allows the user to automate releases of their projects by
leveraging the user's git repositories. Dailypack builds your
project, runs tests, and does whatever else you may want it to do. If each
step of the process is successful, Depending on the templates you select
in your config file, Dailypack may push a ZIP archive of your source to
a fileserver over SSH, release an executable, or simply tell you that 
everything worked right- it's all up to the templates you choose.

The user creates a [REML](https:www.gitlab.com/ryukoposting/REML)-formatted 
configuration file that describes the location of the remote git repository,
as well as each build and test that should be run before a build is validated.
See the sample configuration file [here](https://gitlab.com/ryukoposting/Dailypack/blob/master/configs/pc-gpio.reml)
for an example of a REML build file.

Dailypack is meant to be run on web servers, as its output directory can be
used to easily create release archives.

### Releases

Dailypack manages Dailypack's releases! It spits out the most recent successful
builds [here](http://dailyprog.org/~ryukoposting/projects/Dailypack/).