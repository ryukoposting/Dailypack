package io.ryukoposting.reml

class RemlException(override val message: String): Exception(message)