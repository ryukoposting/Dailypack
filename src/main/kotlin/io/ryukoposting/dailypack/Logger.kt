package io.ryukoposting.dailypack

import java.io.File

object Logger {
    private lateinit var logFile: File

    fun start(logFilePath: String) {
        with(File(logFilePath)) {
            if (createNewFile() || exists()) logFile = this
        }
    }

    fun println(message: Any) {
        if (logFile.exists()) logFile.appendText("$message\n")
        kotlin.io.println(message)
    }

    fun print(message: Any) {
        if (logFile.exists()) logFile.appendText("$message")
        kotlin.io.print(message)
    }
}