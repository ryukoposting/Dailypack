package io.ryukoposting.dailypack

import java.io.File

fun runCommand(command: String, workingDir: File): Int {
    ProcessBuilder().apply {
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            command("cmd.exe", "/c", command)
        } else {
            command("sh", "-c", command)
        }
        directory(workingDir)
        inheritIO()

        val process = start()
        process.waitFor()
        return process.exitValue()
    }
    return -1
}