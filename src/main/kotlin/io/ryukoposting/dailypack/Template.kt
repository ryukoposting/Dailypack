package io.ryukoposting.dailypack

import java.io.File

class Template(templateFile: File, val destinationPath: String) {
    val name: String = templateFile.name.split(".")[0]

    private val templateText = templateFile.readText()

    val requiredProperties: Set<String> = "__.*?__".toRegex().findAll(templateText).map {
        it.value.removeSurrounding("__").toLowerCase() }.toSet()

    fun generateBuildscript(propertyMap: Map<String,Any?>) {
        if (!propertyMap.keys.containsAll(requiredProperties)) {
            throw Exception("Given propertyMap does not contain all required properties!")
        } else {
            var outString = templateText
            propertyMap.forEach { name, value ->
                outString = outString.replace("__${name.toUpperCase()}__", "$value")
            }
            File(destinationPath).writeText(text=outString.replace("\r\n", "\n"))

            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                // if we need to do anything for windows, do it here
            } else {
                runCommand("sudo chmod +x $destinationPath", File("${propertyMap["buildscript_dir"]}"))
            }
        }
    }

}