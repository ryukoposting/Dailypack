package io.ryukoposting.dailypack

import io.ryukoposting.reml.Reml
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun main(args: Array<String>) = if (args.isEmpty()) {
    println("please specify at least one build configuration file!")
} else {
    val timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss"))
    Logger.start("./dailypack-logs/log-$timestamp.txt")
    Logger.println("Opening dailypack logger with timestamp: $timestamp")

    args.forEach { path ->
        try {
            try {
                Reml.loadFromPath(path)
            } catch (e: Exception) {
                Logger.println("\nspecified config file $path could not be loaded")
                null
            }?.also { reml ->
                Logger.println("\nloaded configuration for $path")

                reml["timestamp"] = timestamp
                reml["success"] = 0
                reml["homedir"] = System.getProperty("user.dir")

                Logger.println("current working directory: ${reml["homedir"]}")

                val globalVars = reml.getKeys().filterNot { reml[it] is Reml.SubReml }.map { Pair(it, reml[it]) }.toMap()

                reml.getKeys().filter {
                    reml[it] is Reml.SubReml
                }.map { key ->
                    Logger.println("found buildstep '$key' which refers to template file ${reml[key, "template"]}.sh.template")

                    val templateFile = File("./templates/${reml[key, "template"]}.sh.template")
                    if (!templateFile.exists()) throw Exception("./templates/${reml[key, "template"]}.sh.template not found!")

                    val properties = mutableMapOf<String, Any?>().apply {
                        putAll(globalVars)
                        putAll((reml[key] as Reml.SubReml).toMap())
                    }

                    Template(templateFile, "${"${reml["buildscript_dir"]}".removeSuffix("/")}/$key.sh").apply {
                        if (reml["success"] == 0) {
                            Logger.println("running buildstep $destinationPath...")
                            generateBuildscript(propertyMap = properties)
                            reml["success"] = runCommand(destinationPath, File(reml["buildscript_dir"] as String))
                            Logger.println("buildstep $destinationPath returned ${if (reml["success"] == 0) "SUCCESS" else "ERROR"}")
                            Logger.println("status code=${reml["success"]}")
                            File(destinationPath).deleteOnExit()
                        } else {
                            Logger.println("buildstep will not be run because a previous step failed.")
                        }

                    }
                }
            }
        } catch (exception: Exception) {    // if an exception occurs during a build, this lets it continue to the next build
            Logger.println("An exception was thrown while running buildscript at $path")
            Logger.println("Stacktrace: ${exception.stackTrace}")
            exception.printStackTrace()
        }
    }
}